/*
 * Simple generate random number.
 *
 * hadrihl hadrihilmi@gmail.com
 */
#include <stdio.h> // printf, NULL
#include <time.h> // time
#include <stdlib.h> // srand, rand

int main(int argc, char* argv[]) {

	srand(time(NULL)); // intialize random seeds.
	int r = rand() % 20 + 1; // generate secret number.
	printf("rand = %d\n", r);

	return 0;
}
