CC = gcc
CC_FLAGS = -Wall -g -O2

BIN = a.out

all: $(BIN)

a.out: gen_rand.c 
	$(CC) $(CC_FLAGS) -o $@ $<

clean: 
	$(RM) $(BIN)
